<?php

use App\Core\Request;

define('BASE_PATH', __DIR__ . '/../');

require BASE_PATH . 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(BASE_PATH);
$dotenv->load();
$request = new Request();

require BASE_PATH . 'Helpers/Helpers.php';
require BASE_PATH . 'Routes/web.php';
