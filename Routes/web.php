<?php

use App\Core\Routing\Route;
# use App\Middleware\BlockFirefox;

Route::add('get', '/', 'HomeController@index');

/*Route::add('get', '/', 'HomeController@index', [BlockFirefox::class, BlockChrome::class]);
Route::add(['get', 'post'], '/product/a-new-product/{productId}/comments/{commentId}', 'ProductController@index');
Route::get('/blog/post/{postId}', 'BlogController@post');
Route::add(['put', 'get'], '/blog/articles/{articleId}', function () {
    echo "this is a articles uri closure";
}); */
