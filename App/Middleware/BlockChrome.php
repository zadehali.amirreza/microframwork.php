<?php

namespace App\Middleware;

use App\Middleware\Contract\MiddlewareInterface;
use hisorange\BrowserDetect\Parser as Browser;

class BlockChrome implements MiddlewareInterface
{
    public function handle()
    {
        if (Browser::isChrome()) {
            die('Sorry! we could not presentation for (Chrome) browser. Please try with another browsers again.');
        }
    }
}
