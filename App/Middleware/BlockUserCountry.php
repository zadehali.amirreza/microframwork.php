<?php

namespace App\Middleware;

class BlockUserCountry
{
    private $ip;
    public function __construct($ip)
    {
        $this->ip = $ip;
    }
    public function info()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=" . $this->ip);
        curl_setopt($ch, CURLOPT_HTTPHEADER,  array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}
