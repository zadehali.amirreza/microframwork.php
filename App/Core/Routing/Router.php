<?php

namespace App\Core\Routing;

use App\Core\Request;
use App\Core\Routing\Route;
use App\Exceptions\ClassNotExistsException;
use App\Exceptions\MethodNotExistsException;
use App\Middleware\BlockUserCountry;

class Router
{
    private $request;
    private $routes;
    private $currentRoute;
    private const BASE_CONTROLLER_PATH = '\App\Controllers\\';
    private const BLOCKED_COUNTRY_NAME = 'Canada';

    public function __construct()
    {
        $this->request = new Request();
        $this->routes = Route::routes();
        $this->currentRoute = $this->findCurrentRoute();

        $this->turnOnMiddleware($this->currentRoute);
        $this->BlockUsersFromCountry($this->request->ip()); //'15.235.82.125'
    }
    public function run()
    {
        # 405 : invalid request method method not allowed
        if ($this->invalidRequestMethod($this->request))
            $this->dispatch405();
        # 404 : uri not exists
        if (is_null($this->currentRoute))
            $this->dispatch404();
        $this->dispatch($this->currentRoute);
    }

    private function BlockUsersFromCountry(string $ip)
    {
        $response = new BlockUserCountry($ip);
        $result = $response->info();
        if ($result->geoplugin_countryName === self::BLOCKED_COUNTRY_NAME)
            die('Sorry we can not presentation for your ip address. your location is ' . $result->geoplugin_countryName);
    }
    private function turnOnMiddleware($currentRoute)
    {
        if (is_null($currentRoute))
            return false;
        foreach ($currentRoute as $key => $value) {
            if ($key === 'middleware') {
                foreach ($value as $k => $v) {
                    $obj = new $v();
                    $obj->handle();
                }
            }
        }
    }
    private function findCurrentRoute()
    {
        foreach ($this->routes as $route) {
            if (in_array($this->request->method(), $route['methods']) && $this->regexMatched($route['uri'])) {
                return $route;
            }
        }
        return null;
    }
    private function regexMatched($slug)
    {
        global $request;
        $pattern = "/^" . str_replace(['/', '{', '}'], ['\/', '(?<', '>[-%\w]+)'], $slug) . "$/";
        $result = preg_match($pattern, $this->request->uri(), $matches);
        if ($result < 1)
            return false;
        foreach ($matches as $key => $value) {
            if (!is_int($key)) {
                $request->addRouteParameters($key, $value);
            }
        }
        return true;
    }
    private function invalidRequestMethod(Request $request)
    {
        foreach ($this->routes as $route) {
            if (in_array($request->method(), $route['methods'])) {
                return false;
            }
        }
        return true;
    }
    private function dispatch405()
    {
        header("HTTP/1.0 405 Method Not Allowed");
        echo "HTTP/1.0 405 Method Not Allowed";
        die();
    }
    private function dispatch404()
    {
        header("HTTP/1.0 404 Not Found");
        view('errors.404');
        die();
    }
    private function dispatch($route)
    {
        $action = $route['action'];

        # action : null
        if (is_null($action))
            return;

        # action : clousure
        if (is_callable($action))
            $action(); // call_user_method($action);

        # action : Controller@method
        if (is_string($action))
            $action = explode('@', $action);

        # action : ['Controller','method']
        if (is_array($action)) {
            $className = self::BASE_CONTROLLER_PATH . $action[0];
            $method = $action[1];

            class_exists($className) ?
                $controller = new $className() :
                throw new ClassNotExistsException();

            method_exists($controller, $method) ?
                $controller->{$method}() :
                throw new MethodNotExistsException();
        }
    }
}
