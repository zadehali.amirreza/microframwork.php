<?php

namespace App\Core;

class Request
{
    private $routeParams;
    private $params;
    private $method;
    private $agent;
    private $uri;
    private $ip;
    public function __construct()
    {
        $this->params = $_REQUEST;
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->ip = $_SERVER['REMOTE_ADDR'];
    }
    public function __set($key, $value)
    {
        $this->params[$key] = $value;
    }
    public function __get($key)
    {
        return $this->params[$key];
    }

    public function addRouteParameters($key, $value)
    {
        $this->routeParams[$key] = $value;
    }
    public function getRouteParameters($key)
    {
        return $this->routeParams[$key];
    }
    public function params()
    {
        return $this->params;
    }
    public function method()
    {
        return $this->method;
    }
    public function uri()
    {
        return $this->uri;
    }
    public function agent()
    {
        return $this->agent;
    }
    public function ip()
    {
        return $this->ip;
    }
    public function input($key)
    {
        return $this->params[$key] ?? null;
    }
    public function isset($key)
    {
        return isset($this->params[$key]);
    }
    public function redirect($route)
    {
        header('Location: ' . $route);
        die();
    }
}
