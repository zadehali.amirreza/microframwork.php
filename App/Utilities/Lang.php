<?php

namespace App\Utilities;

class Lang
{
    private const LANG = ['en', 'fa', 'ar'];
    public static function convertNumbersTo(string $string, string $flag = 'en'): string|array
    {
        if (!in_array($flag, self::LANG))
            return $string;

        $numbers = [
            'en' => array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
            'fa' => array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
            'ar' => array('٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠')
        ];
        $replace = $numbers[$flag];
        unset($numbers[$flag]);

        foreach ($numbers as $key => $value) {
            if ($key != $flag) {
                $string = str_replace($value, $replace, $string);
            }
        }
        return $string;
    }
    public static function persianNumbers(string $input)
    {
        $english = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        return str_replace($english, $persian, $input);
    }
    public static function englishNumbers(string $input)
    {
        $english = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        return str_replace($persian, $english, $input);
    }
}
