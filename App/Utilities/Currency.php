<?php

namespace App\Utilities;

class Currency
{
    public static function formatPriceInHezarToman(int $input): int
    {
        return $input / 1000;
    }
    public static function formatPriceInMilionToman(int $input): int
    {
        return $input / 1000000;
    }
    public static function formatPriceInRial(int $input): int
    {
        return $input * 10;
    }
}
