<?php

namespace App\Utilities;

class Assets
{
    public static function __callStatic(string $dirName, array $fileName): string
    {
        return $_ENV['BASE_URL'] . "Assets/$dirName/$fileName[0]";
    }
}
