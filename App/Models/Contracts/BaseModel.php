<?php

namespace App\Models\Contracts;

abstract class BaseModel implements CrudInterface
{
    protected $connection;
    protected $table;
    protected $primaryKey = 'id';
    protected $pageSize = 10;
    protected $attributes = [];
    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
    }
    public function __get($key)
    {
        return $this->attributes[$key];
    }

    public function getAttribute($key = null)
    {
        if (!$key || !array_key_exists($key, $this->attributes)) {
            return $this->attributes;
        }
        return $this->attributes[$key];
    }
}
