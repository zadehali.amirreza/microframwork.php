<?php

namespace App\Models\Contracts;

use App\Models\Contracts\BaseModel;
use Medoo\Medoo;

class MysqlBaseModel extends BaseModel
{
    protected $connection;
    public function __construct()
    {
        try {
            //$this->connection = new \PDO("mysql:host={$_ENV['DB_SERVERNAME']};dbname={$_ENV['DB_NAME']};charset={$_ENV['DB_CHARSET']};port={$_ENV['DB_PORT']}", $_ENV['DB_USER'], $_ENV['DB_PASS']);
            $this->connection = new Medoo([
                // [required]
                'type' => $_ENV['DB_DRIVER'],
                'host' => $_ENV['DB_SERVERNAME'],
                'database' => $_ENV['DB_NAME'],
                'username' => $_ENV['DB_USER'],
                'password' => $_ENV['DB_PASS'],

                // [optional]
                'charset' => $_ENV['DB_CHARSET'],
                // 'collation' => $_ENV['DB_COLLECTION'],
                'port' => $_ENV['DB_PORT'],

                // [optional] The table prefix. All table names will be prefixed as PREFIX_table.
                #'prefix' => 'PREFIX_',

                // [optional] To enable logging. It is disabled by default for better performance.
                #'logging' => true,

                // [optional]
                // Error mode
                // Error handling strategies when the error has occurred.
                // PDO::ERRMODE_SILENT (default) | PDO::ERRMODE_WARNING | PDO::ERRMODE_EXCEPTION
                // Read more from https://www.php.net/manual/en/pdo.error-handling.php.
                #'error' => PDO::ERRMODE_SILENT,

                // [optional]
                // The driver_option for connection.
                // Read more from http://www.php.net/manual/en/pdo.setattribute.php.
                #'option' => [
                #    PDO::ATTR_CASE => PDO::CASE_NATURAL
                #],

                // [optional] Medoo will execute those commands after the database is connected.
                #'command' => [
                #    'SET SQL_MODE=ANSI_QUOTES'
                #]
            ]);
        } catch (\Exception $e) {
            die('Connection faild => ' . $e->getMessage());
        }
    }
    public function save()
    {
        $data = $this->attributes;
        $where = [$this->primaryKey => $this->attributes['id']];
        $this->update($data, $where);
        return $this;
    }
    public function remove()
    {
        $where = [$this->primaryKey => $this->attributes['id']];
        return $this->delete($where);
    }

    # Create
    public function create(array $data): int
    {
        $this->connection->insert($this->table, $data);
        return $this->connection->id();
    }
    # Read
    public function find(int $id): object
    {
        $record = $this->connection->select($this->table, '*', [$this->primaryKey => $id]);
        foreach ($record as $row) {
            foreach ($row as $key => $value) {
                $this->attributes[$key] = $value;
            }
        }
        return $this;
    }
    public function get(array $columns, array $where): array
    {
        return $this->connection->select($this->table, $columns, $where);
    }
    public function getAll(): array
    {
        return $this->connection->select($this->table, '*');
    }
    # Update
    public function update(array $data, array $where): int
    {
        $row = $this->connection->update($this->table, $data, $where);
        return $row->rowCount();
    }
    # Delete
    public function delete(array $where): int
    {
        $row = $this->connection->delete($this->table, $where);
        return $row->rowCount();
    }
}
