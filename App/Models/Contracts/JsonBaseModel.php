<?php

namespace App\Models\Contracts;

use App\Models\Contracts\BaseModel;
use stdClass;

class JsonBaseModel extends BaseModel
{
    private $dbFolder;
    private $tableFilePath;
    public function __construct()
    {
        $this->dbFolder = BASE_PATH . 'Storage/JsonDb/';
        $this->tableFilePath = $this->dbFolder . $this->table . '.json';
    }
    private function getData(): array
    {
        return json_decode(file_get_contents($this->tableFilePath));
    }
    private function writeJsonFile(array $data)
    {
        return file_put_contents($this->tableFilePath, json_encode($data));
    }
    private function selectColumns(array $data, array $indexes)
    {
        return
            sizeof($indexes) ?
            (array)array_intersect_key($data, array_flip($indexes)) :
            $data;
    }

    # Create
    public function create(array $data): int
    {
        $arrayData = $this->getData();
        $arrayData[] = $data;
        return  $this->writeJsonFile($arrayData) ? 1 : 0;
    }
    # Read
    public function find(int $id): object
    {
        $data = $this->getData();
        foreach ($data as $row) {
            if ($row->{$this->primaryKey} == $id)
                return $row;
        }
        return null;
    }
    public function get(array $columns, array $where): array
    {
        foreach ($this->getData() as $row) {
            $vars = get_object_vars($row);
            if (array_intersect_key($where, $vars) && array_intersect($where, $vars)) {
                return $this->selectColumns($vars, $columns);
            }
        }
        return [];
    }
    public function getAll(): array
    {
        return $this->getData();
    }
    # Update
    public function update(array $data, array $where): int
    {
        $baseData = $this->getData();
        foreach ($baseData as $row) {
            $vars = get_object_vars($row);
            if (array_intersect_key($where, $vars) && array_intersect($where, $vars)) {
                foreach ($vars as $key => $value) {
                    if (in_array($key, array_keys($data))) {
                        $row->{$key} = $data[$key];
                        return $this->writeJsonFile($baseData) ? 1 : -1;
                    }
                }
            }
        }
        return 0;
    }
    # Delete
    public function delete(array $where): int
    {
        $counter = 0;
        $baseData = $this->getData();
        foreach ($baseData as $row) {
            $vars = get_object_vars($row);
            if (array_intersect_key($where, $vars) && array_intersect($where, $vars)) {
                unset($baseData[$counter]);
                return $this->writeJsonFile(array_values($baseData)) ? 1 : -1;
            }
            $counter++;
        }
        return 0;
    }
}
