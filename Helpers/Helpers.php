<?php
# siteUrl() is create and return
function siteUrl(string $uri): string
{
    return $_ENV['BASE_URL'] . $uri;
}
function view(string $path)
{
    $path = str_replace('.', '/', $path);
    require realpath(BASE_PATH . "/Views/$path.php");
}
function nice_dd($input)
{
    echo "<pre style='box-sizing:border-box;width:100%;padding:0.1rem 1rem;border:3px solid #af00d4;border-radius:0.5rem;border-left: 0.75rem solid #af00d4;'>";
    var_dump($input);
    echo "</pre>";
}
